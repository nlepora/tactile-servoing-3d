# Pose Models for 3D Surfaces and Edges

A Python library for tactile servo control using pose models built from convolutional neural networks on the tactile images. 

Methods described in 
Pose-Based Tactile Servoing: Controlled Soft Touch with Deep Learning  
N Lepora, J Lloyd (2021) IEEE Robotics & Automation Magazine  
https://arxiv.org/pdf/2012.02504.pdf 

Methods use (x, y, z, rx, ry, rz) components of pose, suitable for >=6-dof robot arms. Here an ABB IRB120 is used.

## Installation

To install the package on Windows or Linux, clone the repository and run the setup script from the repository root directory:

```sh
pip install -e .
```

Code was developed and run from Visual Studio Code.

## Examples

Some examples that demonstrate how to use the library are included in the `\examples` directory.  

- For an ABB IRB120 robot
	```sh
	python abb_robot_test.py
	```

## Requirements

Needs installation of these packages

Common Robot Interface (CRI) fork at  
https://github.com/nlepora/cri

Video Stream Processor (VSP) fork at  
https://github.com/nlepora/vsp 

Pose Models 3D at  
https://bitbucket.org/nlepora/pose-models-3d

## Workflow

Assumes you have already used Pose Models 3D to construct the models or downloaded a repository of data and models. 

0. Optional: check the installation with \examples

New experiments:  

1. Run servo_edge2d, servo_edge3d or servo_surface2d. The poses will be saved in predictions.csv. The data will be saved in frames_bw

Existing data:  

2. You can replay previously collected trajectories either on the physical robot (ABB controller), simulated robot (ABB controller with robot studio) or virtually with no robot (dummy controller)   
3. Optionally you can visualize the contour and tactile images during replay (saved as contour.mp4 or frames.mp4)  
4. Optionally you can replay to use a camera to video the experiment.  

Note: all code requires an environment variable DATAPATH to the data directory.

## Precollected servoing data

Servo control data for use in replay and simulation from using this code are available in the data repository  
tactile-servoing-3d-abb

Available for  
- tactip-127: Hemispherical TacTip (127 pin version; 40mm dia.)

Has 3 types of pose model/data:  
- servo_edge2d: around a horizontal edge of a planar object   
https://bit.ly/3KfBeGh  
- servo_surface2d: over a 3d surface  
https://bit.ly/3OEAG0e   
- servo_edge3d: around the edge of a 3d object  
https://bit.ly/3ESiNGI

## Optional - Simulated ABB Robot

You can replay in simulation using RobotStudio software from ABB. 

1.	Open `Robot Studio` and select `Solution with Empty Station`. Press `Create`.  
2.	Go to `Home->Build Station` tab. Open `ABB Library` menu, select `IRB 120`. Click `OK`.  
3.	Go to `Home->Build Station` tab. Open `Virtual controller` menu, select `New Controller`. In the pop dialogue, select `RobotWare` and `Robot model` version and click `OK`. Wait for station to start. Select `IRB120_3...` and click `OK`.  
4.  Go to `Controller->Virtual Controller` tab and select `Change Options`. In the dialog popup under `Motion Events` select `World Zones`, and under `Communication` select `PC Interface`. Click `OK` then `Yes` to restart the controller.  
5.	In left-hand explorer pane, open `RAPID->T_ROB1`. Right-click on `T_ROB1` and `Load Module`, navigate to `abb_server.mod` in `cri/abb` and open. The program should now appear (you can close it).  
6.	Go to `RAPID->Test and Debug` tab, select `Set Program Pointer to Main in all Tasks`, then click the `Start` button.  
7.  Should be ready to connect via IP address `127.0.0.1`  

Note: without an ABB license you cannot save the station, and so must go through these steps each time Robot Studio is started. Tested on Robot Studio 2019. 

## Papers

The methods and data are described and used in this paper

Pose-Based Tactile Servoing: Controlled Soft Touch with Deep Learning  
N Lepora, J Lloyd (2021) IEEE Robotics & Automation Magazine  
https://arxiv.org/pdf/2012.02504.pdf 

Related papers

Optimal Deep Learning for Robot Touch: Training Accurate Pose Models of 3d Surfaces and Edges  
N Lepora, J Lloyd (2020) IEEE Robotics & Automation Magazine  
https://arxiv.org/pdf/2003.01916.pdf

From Pixels to Percepts: Highly Robust Edge Perception and Contour Following using Deep Learning and an Optical Biomimetic Tactile Sensor  
N Lepora et al (2019) IEEE Robotics & Automation Letters  
https://arxiv.org/pdf/1812.02941.pdf 

## Meta

tactile-servoing-3d:

Nathan Lepora – n.lepora@bristol.ac.uk

[https://bitbucket.org/nlepora/pose-based-tactile-servoing-3d](https://bitbucket.org/nlepora/pose-based-tactile-servoing-3d)

Distributed under the GPL v3 license. See ``LICENSE`` for more information.
