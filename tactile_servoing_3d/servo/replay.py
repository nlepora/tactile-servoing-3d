import os, json

from cri.robot import SyncRobot
from cri.controller import ABBController as Controller
# from cri.controller import DummyController as Controller
from vsp.sensor_replay import SensorReplay as Sensor
# from vsp.sensor_dummy import SensorDummy as Sensor

from tactile_servoing_3d.lib.common import home, control
from tactile_servoing_2d.lib.visualise.plot_frames import PlotFrames
from tactile_servoing_3d.lib.visualise.plot_contour3d import PlotContour

# from pose_models_3d.lib.models.cnn_model_tf1 import CNNmodel as Model
from pose_models_3d.lib.models.dummy_model import DummyModel as Model

data_path = os.path.join(os.environ["DATAPATH"], "open", "tactile-servoing-3d-abb")
replay_dir = os.path.join(data_path, "tactip-127", "servo_surface3d", r"head\run_9")


# Specify directories and files
def main(replay_dir):

    # Load/modify meta data
    with open(os.path.join(replay_dir, "meta.json"), 'r') as f:
        meta = json.load(f)   
    # meta['ip'] = '127.0.0.1' # virtual ABB
    meta['ip'] = '192.168.125.1' # virtual ABB
    meta["linear_speed"], meta["angular_speed"] = [10, 10]

    # Absolute paths
    for key in [k for k in meta.keys() if "file" in k or "dir" in k]:
        meta[key] = os.path.join(data_path, meta[key])

    # Startup/load model
    model = Model()
    model.load_model(**meta)

    # Initialize plots
    figs = []#PlotContour(**meta), PlotFrames()] # inv=-1 for contour2d; can delete **meta for surface3d; replay_dir to save

    # Control robot
    with SyncRobot(Controller(meta['ip'])) as robot, Sensor(**meta) as sensor:       
        home(robot, **meta)     
        pred, _ = control(robot, sensor, model, figures=figs, **meta)  

    # Save run to file
    pred.to_csv(meta["test_df_file"], index=False)


if __name__ == '__main__':
    main(replay_dir)
    