import os, json, numpy as np

from cri.robot import SyncRobot
from cri.controller import ABBController as Controller
from vsp.video_stream import CvImageOutputFileSeq, CvVideoDisplay, CvPreprocVideoCamera   
from vsp.processor import CameraStreamProcessor, AsyncProcessor

from tactile_servoing_3d.lib.common import home, control
from pose_models_3d.lib.models.cnn_model_tf1 import CNNmodel as Model

def make_sensor(size, crop, exposure, source, threshold=None, **kwargs):  # amcap: reset all settings; autoexposure off; saturdation max
    camera = CvPreprocVideoCamera(size, crop, threshold, exposure=exposure, source=source)
    for _ in range(5): camera.read() # Hack - camera transient   
    return AsyncProcessor(CameraStreamProcessor(camera=camera,
            display=CvVideoDisplay(name='sensor'),
            writer=CvImageOutputFileSeq()))

data_path = os.path.join(os.environ["DATAPATH"], "open", "tactile-servoing-3d-abb")


# User-defined paths
model_dir = os.path.join("tactip-127", "model_surface3d", "train", "train", "train3d_cnn_opt")  
with open(os.path.join(data_path, model_dir, "meta_35.json"), 'r') as f: 
    model_meta = json.load(f)

# Batch runs'   
# name = 'rXY'; var = 'r'; vals = [[np.cos(np.pi/4*i), np.sin(np.pi/4*i), 3, 0, 0, 0] for i in range(8)] 
# name = 'rZ'; var = 'r'; vals = [[0, 1, i/2, 0, 0, 0] for i in range(2,9+1)]
# name = 'rPhi'; var = 'r'; vals = [[0, 1, 3, i*5, 0, 0] for i in range(-3,2+1)] 
# name = 'rPsi'; var = 'r'; vals = [[0, 1, 3, 0, i*5, 0] for i in range(-2,2+1)] 
# name = 'rTheta'; var = 'r'; vals = [[np.cos(np.pi/2*i), np.sin(np.pi/2*i), 3, 0, 0, j] for j in [2,-2] for i in range(4)]

# name = 'sphere'; var = 'r'; vals = [[np.cos(np.pi/4*i), np.sin(np.pi/4*i), 3, 0, 0, 0] for i in range(8)] 
# name = 'brain'; var = 'r'; vals = [[np.cos(np.pi/4*i), np.sin(np.pi/4*i), 3, 0, 0, 0] for i in range(8)] 
name = 'head'; var = 'r'; vals = [[-np.sin(np.pi*i/180), np.cos(np.pi*i/180), 3, 0, 0, 0] for i in [*range(-45,-10+1,5),-5+1,*range(0,40+1,5),45-3]]
# name = 'saddle'; var = 'r'; vals = [[np.cos(np.pi/4*i), np.sin(np.pi/4*i), 3, 0, 0, 0] for i in range(8)] 

for i, val in enumerate(vals):        
    test_dir = os.path.join("tactip-127", "servo_surface3d", name, f"run_{i+1}") 

    # Make the new meta dictionary
    meta = {**model_meta, 
        # ~~~~~~~~~ Paths ~~~~~~~~~#
        "meta_file": os.path.join(test_dir, "meta.json"),        
        "test_image_dir": os.path.join(test_dir, "frames_bw"),
        "test_df_file": os.path.join(test_dir, "predictions.csv"),
        # ~~~~~~~~~ Robot movements ~~~~~~~~~#
        "ip": '164.11.72.57', 
        "home_pose": [400, 0, 300, 180, 0, 180],  
        # "work_frame": [348, -110, 239, 180, 0, 180], # sphere
        # "work_frame": [347, -150, 184, 180, 0, 180], # brain
        # "work_frame": [385, -217, 122, 250, 0, 180], # sadde
        "work_frame": [385, -217, 122, 250, 0, 180], # head
        "num_steps": 400, 
        # ~~ other settings ~~
        "source": 0,
        "r": [0, 1, 3, 0, 0, 0], 
        "kp": [1, 1, 0.5, 0.5, 0.5, 1],
        "ki": [0, 0, 0.3, 0.1, 0.1, 0],
        "ei_bnd": [[0, 0, 5, 15, 15, 0], [0, 0, -5, -15, -15, 0]],
        # ~~~~~~~~~ Comments ~~~~~~~~~#
        "comments": "servo control with 3d surface on ABB"
        }

    # Update dictionary
    meta[var] = val

    # Save dictionary to file
    os.makedirs(os.path.join(data_path, test_dir), exist_ok=True)
    with open(os.path.join(data_path, meta["meta_file"]), 'w') as f:
        json.dump(meta, f)

    # Absolute paths
    for key in [k for k in meta.keys() if "file" in k or "dir" in k]:
        meta[key] = os.path.join(data_path, meta[key])

    # Startup/load model 
    model = Model()
    model.load_model(**meta)

    # Control robot with loaded model
    with SyncRobot(Controller(meta['ip'])) as robot, make_sensor(**meta) as sensor:       
        home(robot, **meta)     
        pred, _ = control(robot, sensor, model, **meta)  

    # Save run to file
    pred.to_csv(meta["test_df_file"], index=False)
