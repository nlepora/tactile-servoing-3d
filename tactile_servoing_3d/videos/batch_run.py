import os, time

from tactile_servoing_3d.servo.replay import main as main_replay
from tactile_servoing_3d.videos.make_video import main as main_video


data_path = os.path.join(os.environ["DATAPATH"], "open", "tactile-servoing-3d-abb")

sensors = ["tactip-127"]

# tasks = ["servo_edge2d"]
# shapes = ["clover", "spiral", "volute", "teardrop"]
# shapes = [rf"disk_runs\rTheta\run_{i+1}" for i in range(11)]

# tasks = ["servo_edge3d"]
# shapes = ["saddle", "spiral", "teardrop", "volute", r"wave\run_1", r"wave\run_2"]
# shapes = [rf"lid\run_{i+1}" for i in range(3)]
# shapes = [rf"disk_runs\rTheta\run_{i+1}" for i in range(19)]

tasks = ["servo_surface3d"]
# shapes = [rf"sphere\run_{i+1}" for i in range(8)]
# shapes = [rf"saddle\run_{i+1}" for i in range(8)]
shapes = [rf"head\run_{i+1}" for i in range(5,14,3)]
# shapes = [rf"sphere_runs\rXY\run_{i+1}" for i in range(8)]

for sensor in sensors:
    for shape in shapes:
        for task in tasks:
            replay_dir = os.path.join(data_path, sensor, task, shape)
            # try:
            main_replay(replay_dir)
                # # time.sleep(1)
                # main_video(replay_dir)
            # except:
            #     pass
            