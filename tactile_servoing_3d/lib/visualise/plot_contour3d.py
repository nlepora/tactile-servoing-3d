import os, numpy as np
from turtle import shape, width
import matplotlib, matplotlib.pylab as plt
from matplotlib import animation

from cri.robot import quat2euler, euler2quat, inv_transform

temp_path = os.environ["TEMPPATH"]


class PlotContour:
    def __init__(self, path = temp_path,
                name="contour",  
                frame_rate=10, 
                position=[0, 0, 640, 640],
                poses=[[0,0,0],[0,0,0]],
                r = [0,0], **kwargs):
        self._name = name
        self._path = path
        self._fig = plt.figure(name, figsize=(5, 5))
        self._fig.clear()
        self._fig.subplots_adjust(left=-0.1, right=1.1, bottom=-0.05, top=1.05)
        self._ax = self._fig.add_subplot(111, projection='3d')
        self._ax.azim = 180
        self._ax.plot(np.array(poses)[:,0], np.array(poses)[:,1], np.array(poses)[:,2], ':w')  

        self.r = r[:2]/np.linalg.norm(r[:2])

        mgr = plt.get_current_fig_manager()
        if matplotlib.get_backend()=='TkAgg': mgr.window.wm_geometry('{}x{}'.format(*position[2:]))  
        if matplotlib.get_backend()=='QT5Agg': mgr.window.setGeometry(*position)

        self._writer = animation.writers['ffmpeg'](fps=frame_rate)
        self._writer.setup(self._fig, os.path.join(path, name+'.mp4'), dpi=100)


    def update(self, v, **kwargs):
        v_q = euler2quat([0, 0, 0, *v[-1,3:]], axes='rxyz')
        d_q = euler2quat([*self.r[::-1], 0, 0, 0, 0], axes='rxyz')
        w_q = euler2quat([0, 0, -1, 0, 0, 0], axes='rxyz')
        d = 5*quat2euler(inv_transform(d_q, v_q), axes='rxyz')
        w = 5*quat2euler(inv_transform(w_q, v_q), axes='rxyz')

        self._ax.plot(v[-2:,0], -v[-2:,1], -v[-2:,2], '-r') 
        self._ax.plot(v[-2:,0]+[d[0],-d[0]], -v[-2:,1]-[d[1],-d[1]], -v[-2:,2]-[d[2],-d[2]], '-b', linewidth=0.5) 
        self._ax.plot(v[-2:,0]+[w[0],0], -v[-2:,1]-[w[1],0], -v[-2:,2]-[w[2],0], '-g', linewidth=0.5) 

        lim = np.array([v[:,:3].min(axis=0), v[:,:3].max(axis=0)])   
        m, s = np.mean(lim, axis=0), np.max(np.diff(lim, axis=0))
        self._ax.set_xlim3d([m[0]-s/2, m[0]+s/2])
        self._ax.set_ylim3d([-m[1]-s/2, -m[1]+s/2])
        self._ax.set_zlim3d([-m[2]-s/2, -m[2]+s/2])

        plt.pause(0.0001)
        self._writer.grab_frame()
        self._fig.show()

    def finish(self, flag=''):
        if flag is 'png': self._fig.savefig(os.path.join(self._path, self._name+'.png'), bbox_inches="tight") 
        self._writer.finish() 


def main():
    None

if __name__ == '__main__':
    main()
